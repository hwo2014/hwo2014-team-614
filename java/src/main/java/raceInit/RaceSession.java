package raceInit;

public class RaceSession {
	private int laps;
	private double maxLapTimeMs;
	private boolean quickRace;

	public RaceSession(int laps, double maxLapTimeMs, boolean quickRace) {
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}

	public int getLaps() {
		return laps;
	}

	public double getMaxLapTimeMs() {
		return maxLapTimeMs;
	}

	public boolean isQuickRace() {
		return quickRace;
	}

	@Override
	public String toString() {
		return "	LAPS: " + laps + ",\n		MAX_LAP_TIME_MS: " + maxLapTimeMs + ",\n		QUICK_RACE: " + quickRace;
	}
}
