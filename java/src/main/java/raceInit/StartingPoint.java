package raceInit;

public class StartingPoint {
	private Position position;
	private double angle;

	public StartingPoint(Position position, double angle) {
		this.position = position;
		this.angle = angle;
	}

	public Position getPosition() {
		return position;
	}

	public double getAngle() {
		return angle;
	}

	@Override
	public String toString() {
		return position + "\n		ANGLE: " + angle;
	}

}
