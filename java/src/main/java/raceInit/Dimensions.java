package raceInit;

public class Dimensions {
	private double length;
	private double width;
	private double guideFlagPosition;

	public Dimensions(double length, double width, double guideFlagPosition) {
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}

	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	public double getGuideFlagPosition() {
		return guideFlagPosition;
	}

	@Override
	public String toString() {
		return "			LENGTH: " + length + ",\n			WIDTH: " + width + ",\n			FLAG_POSITION: " + guideFlagPosition;
	}
}
