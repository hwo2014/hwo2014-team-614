package raceInit;

public class GameInit {
	private InitObject data;

	public GameInit(InitObject data) {
		this.data = data;
	}

	public InitObject getData() {
		return data;
	}

	@Override
	public String toString() {
		return data + "";
	}
}
