package raceInit;

public class Cars {
	private Id id;
	private Dimensions dimensions;

	public Cars(Id id, Dimensions dimensions) {
		this.id = id;
		this.dimensions = dimensions;
	}

	public Id getId() {
		return id;
	}

	public Dimensions getDimensions() {
		return dimensions;
	}

	@Override
	public String toString() {
		return "		ID:" + id + ",\n		DIMENSIONS: " + dimensions;
	}
}
