package raceInit;

public class InitObject {
	private Race race;

	public InitObject(Race race) {
		this.race = race;
	}

	public Race getRace() {
		return race;
	}

	@Override
	public String toString() {
		return race + "\n}";
	}
}
