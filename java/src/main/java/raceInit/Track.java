package raceInit;

public class Track {
	private String id;
	private String name;
	private Piece[] pieces;
	private Lanes[] lanes;
	private StartingPoint startingPoint;

	public Track(String id, String name, Piece[] pieces, Lanes[] lanes, StartingPoint startingPoint) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.startingPoint = startingPoint;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Piece[] getPieces() {
		return pieces;
	}

	public Lanes[] getLanes() {
		return lanes;
	}

	public StartingPoint getStartingPoint() {
		return startingPoint;
	}

	@Override
	public String toString() {
		String pString = "ID: " + id + ",\n	NAME: " + name + ",\n	PIECES: [\n";

		for (Piece curPiece : pieces)
			pString += "	{\n		LENGTH: " + curPiece.getPieceLength() + "\n		SWITCH: " + curPiece.isSwitched() + "\n		RADIUS: " + curPiece.getRadius() + "\n		ANGLE: " + curPiece.getAngle() + "\n	},\n";

		// pString = pString.substring(0, pString.length()-3);
		pString += "\n	}\n	],\n	LANES: [\n";

		for (Lanes curLane : lanes)
			pString += "	{\n		DISTANCE_FROM_CENTER: " + curLane.getOffset() + "\n		INDEX: " + curLane.getIndex() + "\n	},\n";

		pString = pString.substring(0, pString.length() - 2);
		pString += "\n	],\n	STARTING_POINT:\n	{\n	" + startingPoint + "\n	}";

		return pString;
	}
}
