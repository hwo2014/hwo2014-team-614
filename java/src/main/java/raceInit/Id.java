package raceInit;

public class Id {
	private String name;
	private String color;

	public Id(String name, String color) {
		this.name = name;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	@Override
	public String toString() {
		return "		NAME: " + name + ",\n			COLOR: " + color;
	}

}
