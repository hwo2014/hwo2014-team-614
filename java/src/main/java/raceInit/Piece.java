package raceInit;

public class Piece {
	private Track track;
	private double length;
	private boolean _switch;
	private double radius;
	private double angle;

	public Piece(double length, boolean _switch, double radius, double angle) {
		this.length = length;
		this._switch = _switch;
		this.radius = radius;
		this.angle = angle;
	}

	// TODO Implement LaneLength caching
	public double getLaneLength(int index) {
		if (length == 0d && angle != 0) {
			double laneOffset = track.getLanes()[index].getOffset();
			double laneRadius = radius + laneOffset;
			return Math.PI * laneRadius * 2 * Math.abs(angle) / 360;
		}
		return length;
	}

	public double getPieceLength() {
		return length;
	}

	public boolean isSwitched() {
		return _switch;
	}

	public double getRadius() {
		return radius;
	}

	public double getAngle() {
		return angle;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	@Override
	public String toString() {
		return "LENGTH: " + length + ",\nSWITCH: " + _switch + ",\nRADIUS: " + radius + ",\nANGLE: " + angle;
	}
}
