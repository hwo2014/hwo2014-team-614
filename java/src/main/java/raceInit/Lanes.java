package raceInit;

public class Lanes {
	private double distanceFromCenter;
	private int index;

	public Lanes(double distanceFromCenter, int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}

	public double getOffset() {
		return distanceFromCenter;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return "DISTANCE_FROM_CENTER: " + distanceFromCenter + ",\nINDEX: " + index;
	}
}
