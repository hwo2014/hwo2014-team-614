package raceInit;

public class Race {
	private Track track;
	private Cars[] cars;
	private RaceSession raceSession;

	public Race(Track track, Cars[] cars, RaceSession raceSession) {
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}

	public Track getTrack() {
		return track;
	}

	public Cars[] getCars() {
		return cars;
	}

	public RaceSession getRaceSession() {
		return raceSession;
	}

	@Override
	public String toString() {
		// return "\nTRACK: { \n	" + track + "\n},\nCARS: [\n	{\n		" + cars + "\n	}\n],\nRACESESSION: { \n	" + raceSession + "\n	}\n";

		String pString = "\nTRACK: {\n	" + track + "\n},\nCARS: [\n";

		for (Cars curCar : cars)
			pString += "	{\n		ID: { \n	" + curCar.getId() + "\n		},\n		DIMENSIONS: { \n" + curCar.getDimensions() + "\n		},";

		pString = pString.substring(0, pString.length() - 1);
		pString += "\n	}\n],\nRACESESSION: {\n	" + raceSession + "\n	}";

		return pString;

	}
}
