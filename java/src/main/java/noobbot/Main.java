package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

import raceInit.GameInit;
import raceInit.Piece;
import raceInit.Track;
import updateInfo.CarObject;
import updateInfo.CarPositions;

import com.google.gson.Gson;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		boolean isCustomRace = false; // Set to true in order to create a custom race

		if (isCustomRace) {
			String trackName = "germany"; // You can choose between "keimola" (default) and "germany"
			int botCount = 1; // Number of bots to join your race. If it is > 1, then you might need to wait for somebody to connect
			JoinCustomRace customRace = new JoinCustomRace(new BotId(botName, botKey), trackName, botCount);

			new Main(reader, writer, new Join(botName, botKey), customRace);
		} else
			new Main(reader, writer, new Join(botName, botKey), null);

		socket.close();
	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	public boolean isFirst;
	public boolean raceStarted = false;

	public Main(final BufferedReader reader, final PrintWriter writer, final Join join, final JoinCustomRace joinCustom) throws IOException {
		this.writer = writer;
		String line = null;

		if (joinCustom != null)
			send(joinCustom);
		else
			send(join);

		GameInit init = null;
		HashMap<Integer, CarObject> hm = new HashMap<Integer, CarObject>();

		Velocity ourVelocity = null;
		CarStabilizer angleStabilizer = new CarStabilizer(0d);

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

			// TODO: Implement AI for switching lanes. See LaneSwitch.java
			if (isFirst) {
				isFirst = false;
				send(new SwitchLane("Right"));
			}
			if (msgFromServer.msgType.equals("carPositions")) {
				CarPositions carPos = gson.fromJson(line, CarPositions.class);
				int curTick = carPos.getGameTick();

				if (curTick % 100 == 0) {
					System.out.println("\nLine number: " + curTick);
					System.out.println("Current HM size: " + hm.size());
				}

				//if (curTick % 2 == 0)
				//	System.out.print('.');

				{ // Workaround for changing lanes [To be deleted]
					int posIndex = carPos.getCarObject()[0].getPiecePosition().getPieceIndex();
					switch (posIndex) {
					case (6):
						send(new SwitchLane("Left"));
						break;
					case (13):
						angleStabilizer.setRecuiredSpeed(0d);
						break;
					case (14):
						angleStabilizer.clearConstSpeed();
						break;
					case (17):
						send(new SwitchLane("Right"));
						break;
					}
				}

				CarObject ourCar = null;
				for (CarObject curCar : carPos.getCarObject())
					if (curCar.getId().getName().equals("SwDT")) { // TODO add possibility to track other cars
						hm.put(curTick, curCar); // FIXME why do we need HashMap anyway?
						ourCar = curCar;
						if (ourVelocity == null) // initialize only once
							ourVelocity = new Velocity(init.getData().getRace().getTrack(), curCar, curCar); // XXX perhaps we do not need to have two parameters?
					}

				double vel = ourVelocity.getVelocity(hm.get(curTick), curTick);

				//if (curTick % 9 == 0)
				System.out.println("Speed		:" + vel);

				/*
				 * for(CarObject curCar : carPos.data) { System.out.println("id: " + curCar.id + " | "); System.out.print("angle: " + curCar.angle + " | "); System.out.print("piecePosition: " + curCar.piecePosition + " | "); System.out.print("gameId: " + carPos.gameId + " | ");
				 * System.out.print("gameTick: " + carPos.gameTick + " | "); }
				 */

				double requiredSpeed = angleStabilizer.getRecuiredSpeed(ourCar.getAngle(), vel);

				if (vel < requiredSpeed)
					send(new Throttle(1));
				else
					send(new Throttle(0));

				// send(new Throttle(0.6539));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("\nJoined");
				// TODO Create custom track with custom number of bots using "createRace" msgType
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("\nRace init");
				isFirst = true;

				// Krivizopi blin
				line = line.replaceAll("switch", "_switch");

				init = gson.fromJson(line, GameInit.class);
				System.out.println(init);

				Track curTrack = init.getData().getRace().getTrack();

				for (int i = 0; i < curTrack.getLanes().length; i++) {
					System.out.println();
					for (int a = 0; a < curTrack.getPieces().length; a++) {
						Piece curPiece = curTrack.getPieces()[a];
						curPiece.setTrack(curTrack);
						System.out.println("Lane: " + (i + 1) + " | " + curPiece.getLaneLength(i));
					}
				}

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("\nRace end");
				System.out.println("Total ticks: " + hm.size());
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("\nRace start");
			} else {
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();

}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class JoinCustomRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final int carCount;

	JoinCustomRace(final BotId botId, final String trackName, final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class SwitchLane extends SendMsg {
	private String direction;

	public SwitchLane(String direction) {
		this.direction = direction;
	}

	@Override
	protected Object msgData() {
		return direction;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}
