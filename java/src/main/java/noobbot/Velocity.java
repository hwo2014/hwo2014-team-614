package noobbot;

import raceInit.Track;
import updateInfo.CarObject;

public class Velocity {
	Track track;
	private int curPieceIndex;
	private int prevPieceIndex;
	private int curLap;
	private int prevLap;
	private double curInPieceDistance;
	private double prevInPieceDistance;
	private int prevTicks = 0;

	public Velocity(Track track, CarObject currentData, CarObject previousData) {
		this.track = track;
		updateData(currentData);
		prevPieceIndex = previousData.getPiecePosition().getPieceIndex();
		prevLap = previousData.getPiecePosition().getLap();
		prevInPieceDistance = previousData.getPiecePosition().getInPieceDistance();
	}

	public void updateData(CarObject currentData) {
		curPieceIndex = currentData.getPiecePosition().getPieceIndex();
		curLap = currentData.getPiecePosition().getLap();
		curInPieceDistance = currentData.getPiecePosition().getInPieceDistance();
	}

	public double getVelocity(CarObject currentData, int curTicks) {
		// TODO sometimes inPieceDistance is bigger than middle piece length. Use lane shift to calculate segment length more precisely.
		updateData(currentData);
		double distance = 0;
		int time = curTicks - prevTicks;
		prevTicks = curTicks;

		// FIXME Sometimes speed is negative value. Needs to be fixed.
		while (prevPieceIndex != curPieceIndex || prevLap != curLap) {
			distance += track.getPieces()[prevPieceIndex].getPieceLength() - prevInPieceDistance;
			if (++prevPieceIndex >= track.getPieces().length) { // piece overflow
				prevPieceIndex = 0;
				prevLap++;
			}
			prevInPieceDistance = 0;
		}

		distance += curInPieceDistance - prevInPieceDistance;
		prevInPieceDistance = curInPieceDistance;

		return distance / time;
	}
}
