package noobbot;

public class BotId {
	public final String name;
	public final String key;

	BotId(String name, String key) {
		this.name = name;
		this.key = key;
	}
}
