package noobbot;

public class CarStabilizer {
	private double prevAngle;
	double requiredSpeed; // TODO find out required speed
	boolean isConst = false;

	public CarStabilizer(double prevAngle) {
		this.prevAngle = prevAngle;
	}

	public double getPrevAngle() {
		return prevAngle;
	}

	public void setRecuiredSpeed(double value) {
		requiredSpeed = value;
		isConst = true;
	}

	public void clearConstSpeed() {
		isConst = false;
	}

	public double getRecuiredSpeed(double curAngle, double curVelocity) {
		if (!isConst) {
			requiredSpeed = 7.5;
			double angleAcceleration = curAngle - prevAngle;

			// double clampSpeed = 6.5;
			// requiredSpeed -= Math.abs(init.getData().getRace().getTrack().getPieces()[ourCar.getPiecePosition().getPieceIndex()].getRadius() / 100);
			// if (requiredSpeed < clampSpeed)
			// requiredSpeed = clampSpeed;

			if (angleAcceleration * curAngle > 0) { // it is getting worse...
				if (Math.abs(curAngle) > 55) // extreme angle :(
					requiredSpeed = 0;
			} else { // we're recovering. Speed up!
				if (Math.abs(curAngle) > 0.5)
					requiredSpeed = 10;
			}

			if (Math.abs(angleAcceleration) > 2.1) // drifting badly :(
				requiredSpeed = 0;
		}

		System.out.println("Required	:" + requiredSpeed);
		System.out.println("CarAngle	:" + curAngle);
		System.out.println();

		prevAngle = curAngle;

		return requiredSpeed;
	}

}
