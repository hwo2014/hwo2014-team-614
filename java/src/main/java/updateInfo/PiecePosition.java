package updateInfo;

public class PiecePosition {
	private int pieceIndex;
	private double inPieceDistance;
	private Lane lane;
	private int lap;

	public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public Lane getLane() {
		return lane;
	}

	public int getLap() {
		return lap;
	}
}
