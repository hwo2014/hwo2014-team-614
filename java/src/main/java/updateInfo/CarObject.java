package updateInfo;

import raceInit.Id;

public class CarObject {
	private Id id;
	private double angle;
	private PiecePosition piecePosition;

	public CarObject(Id id, double angle, PiecePosition piecePosition) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}

	public Id getId() {
		return id;
	}

	public double getAngle() {
		return angle;
	}

	public PiecePosition getPiecePosition() {
		return piecePosition;
	}

}
