package updateInfo;

public class Lane {
	private int startLaneIndex;
	private int endLaneIndex;

	public Lane(int startLaneIndex, int endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}

	public int getEndLaneIndex() {
		return endLaneIndex;
	}
}
