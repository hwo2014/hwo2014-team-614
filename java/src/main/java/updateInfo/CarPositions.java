package updateInfo;

public class CarPositions {
	private CarObject[] data;
	private String gameId;
	private int gameTick;

	public CarPositions(CarObject[] data, String gameId, int gameTick) {
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}

	public CarObject[] getCarObject() {
		return data;
	}

	public String getGameId() {
		return gameId;
	}

	public int getGameTick() {
		return gameTick;
	}
}